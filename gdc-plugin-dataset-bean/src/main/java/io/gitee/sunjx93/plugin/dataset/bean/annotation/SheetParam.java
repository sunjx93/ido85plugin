package io.gitee.sunjx93.plugin.dataset.bean.annotation;

import java.lang.annotation.*;

@Target({ElementType.PARAMETER,ElementType.ANNOTATION_TYPE,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface SheetParam {
    String value();
}
