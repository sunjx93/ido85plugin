## 程序数据集使用

[maven jar 推送教程](https://blog.csdn.net/yuanlaijike/article/details/106881684)

* demo: `gdc-plugin-dataset-provider`

### 0.依赖条件

> 依赖 springboot/springcloud web(webflux) 集成

### 1. maven(jar) 引用

```xml

<dependency>
    <groupId>io.gitee.sunjx93</groupId>
    <artifactId>gdc-plugin-dataset-bean</artifactId>
    <version>0.0.5</version>
</dependency>
```

```xml

<repository>
    <id>central</id>
    <name>Maven Repository Switchboard</name>
    <url>https://repo1.maven.org/maven2/</url>
</repository>
```

### 2.使用方式

* 目前支持
    * map类型传参（单参）
    * `@SheetParam` 注解声明入参 (基本数据类型与可被序列化的简单java 类型)

* 方法返回体为 Collection以及Collection子类

##### 2.1 map 传参

* 方法体

```java
public List<DemoItem> list(Map<String, Object> map){
        return xx;
        }
```

* 报表入参

``` 
    name - xxx
    age - 123
```

##### 2.2 注解声明

> com.ido85.plugin.dataset.bean.annotation.SheetParam

@SheetParam 目前支持基本类型和封装类型

* 方法体

```java
 public List<DemoItem> listParam(@SheetParam("name") String name,@SheetParam("item") DemoItem item){
        return result;
        }
```

* 报表入参 ，当目标为封装类型时，传参为**未转义**的json体文本

```
    name - xx
    item - "{"id":0,"name":"张三","age":12,"createTime":"2022-04-01 14:26:12"}"
```

##### 2.3 自定义类型

**目前自定义类型只支持单个参数**

```java
public List<DemoItem>  listItemResult(DemoItem item){
    return result;
}
```

### 3.特殊情况

* 假如只想返回某个java class的成员变量，可以使用`@SheetResult`注解进行特别声明，用`.`分割，如

结构体：

```java

@Data
public class DemoStruct {
    private List<DemoItem> result;
}
```

方法体：

```java
@SheetResult(resultName = "result")
public DemoStruct listItemResult(@SheetParam("name") String name,@SheetParam("item") DemoItem item){
    DemoStruct struct=new DemoStruct();
    struct.setResult(this.listByParamItem(name,item));
    return struct;
}
```
