package io.gitee.sunjx93.plugin.dataset.bean.util;


import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

/**
 * @author ：sunjx
 */
public class FieldUtil {

    /**
     * 获取private 修饰的变量
     *
     * @param o refluct param
     * @param valueName value name
     * @return private field value
     */
    public static Object getPrivateValue(Object o, String valueName) {
        Field field = ReflectionUtils.findField(o.getClass(), valueName);
        field.setAccessible(true);
        try {
            Object o1 = field.get(o);
            return o1;

        } catch (
                Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
