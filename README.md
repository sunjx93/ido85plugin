# ido85plugin

#### 基于spring 暴露bean 调用入口的插件 

### 1.原理
* 暴露 /rest 接口
* 请求体携带beanName 与 methodName，方法入参
* 根据反射调用具体bean的方法


### 2.使用
[link](./gdc-plugin-dataset-bean/readme.md)