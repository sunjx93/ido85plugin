package io.gitee.sunjx93.plugin.dataset.bean;

import io.gitee.sunjx93.plugin.dataset.bean.pojo.BeanDsResult;
import io.gitee.sunjx93.plugin.dataset.bean.pojo.BeanQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.InvocationTargetException;

@RestController
public class DsBeanController {

    /**
     * 方法上加@WebMentod(exclude=true)后，此方法不被发布；
     *
     * @param name
     * @return
     */
    @Autowired
    private BeanDatasetDefinition definition;

    @PostMapping("/plugin/ds/bean")
    public BeanDsResult doRequest(@RequestBody BeanQuery beanQuery) throws InvocationTargetException, IllegalAccessException {
        BeanDsResult objects = definition.queryData(beanQuery);
        return objects;
    }

}
