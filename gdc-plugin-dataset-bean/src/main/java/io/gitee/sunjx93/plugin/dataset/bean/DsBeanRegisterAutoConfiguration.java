package io.gitee.sunjx93.plugin.dataset.bean;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Orange
 */
@ConditionalOnProperty(prefix = "gdc.plugin", name = "dsBean", havingValue = "true")
@Configuration
public class DsBeanRegisterAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public BeanDatasetDefinition restRegister() {
        return new BeanDatasetDefinition();
    }

}
