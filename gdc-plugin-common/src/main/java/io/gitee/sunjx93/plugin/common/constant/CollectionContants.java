package io.gitee.sunjx93.plugin.common.constant;


import java.util.HashMap;
import java.util.Map;

public class CollectionContants {


    //开启定时同步
    public static final int SYNC_YES = 0;
    //关闭定时同步
    public static final int SYNC_NO = 1;
    public static final int BATCHSIZE = 10000;

    public static final int resultNum = 100;
    public static final int DB_TYPE_MYSQL = 1;
    public static final int DB_TYPE_TIMESCALEDB = 2;

    //数据源 是否启用 1 启用 0 停用
    public static final int DATASOURCE_ISUSE_YES = 1;
    public static final int DATASOURCE_ISUSE_NO = 0;


    //系统数据类型 1数字 2文本 3日期
    public static final int BDP_DATA_TYPE_NUMBER = 1;
    public static final int BDP_DATA_TYPE_TEXT = 2;
    public static final int BDP_DATA_TYPE_DATETIME = 3;

    public static final String BDP_DATA_TYPE_NUMBER_STR = "1";
    public static final String BDP_DATA_TYPE_TEXT_STR = "2";
    public static final String BDP_DATA_TYPE_DATETIME_STR = "3";

    public static final Map<Integer, String> BDP_COLUM_TYPE_MAP = new HashMap<Integer, String>(){
        {
            put(1, "NUMBER");
            put(2, "TEXT");
            put(3, "DATE");
        }
    };

    public static final Map<Integer, Integer> BDP_COLUM_TYPE_LENGTH_MAP = new HashMap<Integer, Integer>(){{
        put(2, 1024);
    }};

    public static final long ENTITY_TYPE_COLLECT = 0;
    public static final String AZK_SUCCESS = "success";
    public static final String AZK_ERROR = "error";
    public static final String AZK_NOLOGIN = "no login";

    /**
     * excel 操作类型 1 追加 2 替换 3 部分替换  4创建
     */
    public static final int EXCEL_HANDEL_APPEND = 1;
    public static final int EXCEL_HANDEL_REPLACE = 2;
    public static final int EXCEL_HANDEL_SINGLEREPLACE = 3;
    public static final int EXCEL_HANDEL_CRATE = 4;
    public static final int EXCEL_HANDEL_DELETE = 5;

    /**
     * db type 操作增量和全量
     */
    public static final int DB_HANDEL_INCREMENT = 2;
    public static final int DB_HANDEL_ALL = 1;

    /**
     * db type 同步方式 1定时 2 暂停
     */
    public static final int SYNC_FIXED_TIME = 1;
    public static final int SYNC_PAUSE = 2;


    public static final long SYSTEM_USER_ID = -99999L;

//    public static final  String FTP_PATH = "/data/sftp/data-dir/gdc/goldeer/excels/";

    public static final String FROM_TYPE_FTP = "FTP";
    public static final String FROM_TYPE_EXCEL = "EXCEL";
    public static final String FROM_TYPE_MYSQL = "MYSQL";
    public static final String FROM_TYPE_SQLSERVER = "SQLSERVER";
    public static final String FROM_TYPE_ORACLE = "ORACLE";
    public static final String FROM_TYPE_TIMESCALEDB = "TIMESCALEDB";
    public static final String FROM_TYPE_POSTGRESQL = "POSTGRESQL";
    // timescaledb
    public static final String FROM_TYPE_TSDB = "TSDB";
    public static final String TO_TYPE_MYSQL = "MYSQL";
    public static final String TO_TYPE_GREENPLUM = "GREENPLUM";
    public static final String EXCEL_TO_MYSQL = "EXCEL-MYSQL";
    public static final String MYSQL_TO_MYSQL = "MYSQL-MYSQL";
    public static final String FTP_TO_GREENPLUM = "FTP-GREENPLUM";
    public static final String EXCEL_TO_GREENPLUM = "EXCEL-GREENPLUM";
    public static final String MYSQL_TO_GREENPLUM = "MYSQL-GREENPLUM";
    public static final String ORACLE_TO_GREENPLUM = "ORACLE-GREENPLUM";
    public static final String SQLSERVER_TO_GREENPLUM = "SQLSERVER-GREENPLUM";
    public static final String POSTGRESQL_TO_GREENPLUM = "POSTGRESQL-GREENPLUM";
    public static final String TIMESCALEDB_TO_GREENPLUM = "TIMESCALEDB-GREENPLUM";

    // timescaledb 时序表的信息存储表所在数据库模式
    public static final String TS_HYPER_SCHEMA = "_timescaledb_catalog";
    // timescaledb 时序表的信息存储表
    public static final String TS_HYPER_TABLE = "hypertable";
    // timescaledb 时序表的时序字段信息存储表
    public static final String TS_HYPER_COLUMN_TABLE = "dimension";

    /* 模板参数-模板参数值-JSON键名称
        val:值
        dynamicFlag:动态值标识（目前仅时间有动态类型）
        format:时间格式
        timeStamp:时间戳*/
    public static final String REPORT_TEMPLATE_PARAM_JSON_VAL = "val";
    public static final String REPORT_TEMPLATE_PARAM_JSON_DYNAMIC = "dynamicFlag";
    public static final String REPORT_TEMPLATE_PARAM_JSON_FORMAT = "format";
    public static final String REPORT_TEMPLATE_PARAM_JSON_STAMP = "timeStamp";


}
