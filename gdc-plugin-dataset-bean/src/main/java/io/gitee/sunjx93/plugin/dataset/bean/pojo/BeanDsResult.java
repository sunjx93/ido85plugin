package io.gitee.sunjx93.plugin.dataset.bean.pojo;


import java.util.Collection;
import java.util.List;

public class BeanDsResult {
    private String msg = "ok";
    private Collection<?> data;
    private List<ResultStruct> structs;

    public Collection<?> getData() {
        return data;
    }

    public void setData(Collection<?> data) {
        this.data = data;
    }

    public List<ResultStruct> getStructs() {
        return structs;
    }

    public void setStructs(List<ResultStruct> structs) {
        this.structs = structs;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class ResultStruct {
        // fieldName
        private String fName;
        // field type 按参数格式转
        private String fType;
        // java 类型
        private String fClassName;
        // 排序
        private int sort;

        public String getfName() {
            return fName;
        }

        public void setfName(String fName) {
            this.fName = fName;
        }

        public String getfType() {
            return fType;
        }

        public void setfType(String fType) {
            this.fType = fType;
        }

        public String getfClassName() {
            return fClassName;
        }

        public void setfClassName(String fClassName) {
            this.fClassName = fClassName;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }
    }

}
