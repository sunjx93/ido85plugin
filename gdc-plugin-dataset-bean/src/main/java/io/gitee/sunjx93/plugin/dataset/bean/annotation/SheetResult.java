package io.gitee.sunjx93.plugin.dataset.bean.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SheetResult {

    /**
     * 返回体的嵌套层级，最后一层必须为 Collection
     * @return result name
     */
    String resultName() default "";

}
